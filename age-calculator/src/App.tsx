import { useState } from "react";
import Display from "./components/Display";
import Inputs from "./components/Inputs";

import "./App.css"

const App = () => {
  const [dates, setDates] = useState({
    day: "",
    month: "",
    year: "",
  })

  const [datesDisplay, setDatesDisplay] = useState({
    day: "",
    month: "",
    year: "",
  })
  return (
    <>
      <Inputs setDates={setDates} dates={dates} setDatesDisplay={setDatesDisplay}/>
      <Display dates={datesDisplay}/>
    </>
  );
};

export default App;
