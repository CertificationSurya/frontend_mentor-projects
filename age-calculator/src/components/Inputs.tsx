import { useEffect, useState } from "react";
import ArrowIcon from "../assets/ArrowIcon";

type InputsProps = {
  setDates: React.Dispatch<
    React.SetStateAction<{
      day: string;
      month: string;
      year: string;
    }>
  >;
  setDatesDisplay: React.Dispatch<
    React.SetStateAction<{
      day: string;
      month: string;
      year: string;
    }>
  >;
  dates: {
    day: string ;
    month: string ;
    year: string ;
  };
};

const defaultError = {
  dayE: "",
  monthE: "",
  yearE: "",
};

let errorOccured = false;

const Inputs = ({ setDates, dates, setDatesDisplay }: InputsProps) => {
  const [error, setError] = useState(defaultError);

  const validateDate = (date: typeof dates) => {
    let dayErr = "";
    let mthErr = "";
    let yearErr = "";

    const { day, month, year } = date;
    const nDay = Number(day);
    const nMonth = Number(month);
    const nYear = Number(year);

    if (nDay > 31 || nDay < 1) {
      dayErr = "Must be a valid day";
      errorOccured = true;
    }
    if (nMonth > 12 || nMonth < 1) {
      mthErr = "Must be a valid month";
      errorOccured = true;
    }
    if (nYear > new Date().getFullYear()) {
      yearErr = "Must be in the past";
      errorOccured = true;
    }
    if (nYear < 1) {
      yearErr = "Must be a valid year";
      errorOccured = true;
    }

    setError({ dayE: dayErr, monthE: mthErr, yearE: yearErr });
  };

  useEffect(() => {
    errorOccured = false;
    setDatesDisplay(
      {} as {
        day: string;
        month: string;
        year: string;
      }
    );
    setError(defaultError);
  }, [dates]);

  return (
    <section className="input-wrapper">
      <div className="inputs">
        <div className={`${error.dayE ? "error" : ""} date day`}>
          <label htmlFor="day">DAY</label>

          <input
            type="number"
            id="day"
            placeholder="DD"
            onChange={(e) => {
              const day = e.target.value;
              setDates({ ...dates, day });
            }}
          />
          <p>{error.dayE}</p>
        </div>

        <div className={`${error.monthE ? "error" : ""} date month`}>
          <label htmlFor="month">MONTH</label>

          <input
            type="number"
            id="month"
            placeholder="MM"
            onWheel={(e) => e.preventDefault()}
            onChange={(e) => {
              const month = e.target.value;
              setDates({ ...dates, month });
            }}
          />
          <p>{error.monthE}</p>
        </div>

        <div className={`${error.yearE ? "error" : ""} date year`}>
          <label htmlFor="year">YEAR</label>

          <input
            type="number"
            id="year"
            placeholder="YYYY"
            onChange={(e) => {
              const year = e.target.value;
              setDates({ ...dates, year });
            }}
          />
          <p>{error.yearE}</p>
        </div>
      </div>

      <div className="icon">
        <ArrowIcon
          onClick={() => {
            validateDate(dates);

            if (errorOccured) {
              return;
            }
            setDatesDisplay({ ...dates });
          }}
        />
      </div>
    </section>
  );
};

export default Inputs;
