type DisplayType = {
  dates: {
    day: string | number;
    month: string | number;
    year: string | number;
  };
};

const Display = ({ dates }: DisplayType) => {
  let { day, month, year } = dates;

  year = Number(year);
  month = Number(month);
  day = Number(day);

  const displayDay =  31 - day;
  const displayMonth = 12 - month;
  const displayYear = new Date().getFullYear()  - year;

  return (
    <section className="displayField">
      <h2 className="list">
        <span>{displayYear ? displayYear : "--"} </span>
        years
      </h2>
      <h2 className="list">
        <span>{displayMonth ? displayMonth : "--"} </span>
        months
      </h2>
      <h2 className="list">
        <span>{displayDay ? displayDay : "--"} </span>
        days
      </h2>
    </section>
  );
};

export default Display;
