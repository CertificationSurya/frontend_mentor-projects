import React, { SVGProps } from "react";
interface ArrowIconProps extends SVGProps<SVGSVGElement> {}

export const ArrowIcon: React.FC<ArrowIconProps> = (props) => (
  <div className="svg-wrapper">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={46}
      height={44}
      {...props}
      className="arrow-icon"
    >
      <path
        fill="none"
        stroke="#FFF"
        strokeWidth={2}
        d="M1 22.019C8.333 21.686 23 25.616 23 44m0 0V0m22 22.019C37.667 21.686 23 25.616 23 44"
      />
    </svg>
  </div>
);
export default ArrowIcon;
